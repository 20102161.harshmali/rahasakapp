package com.rahasak.rahasakapp.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.rahasak.rahasakapp.pojo.Biller;

import java.util.ArrayList;

public class BillerSource {

    public static void createBiller(Context context, Biller biller) {
        SQLiteDatabase db = SenzorsDbHelper.getInstance(context).getWritableDatabase();

        // content values to inset
        ContentValues values = new ContentValues();
        values.put(SenzorsDbContract.Biller.COLUMN_NAME_BILL_TYPE, biller.getBillType());
        values.put(SenzorsDbContract.Biller.COLUMN_NAME_BILLER_NAME, biller.getBillerName());
        values.put(SenzorsDbContract.Biller.COLUMN_NAME_BILLER_ACCOUNT, biller.getBillerAccount());

        // insert the new row, if fails throw an error
        db.insertOrThrow(SenzorsDbContract.Biller.TABLE_NAME, null, values);
    }

    public static ArrayList<Biller> getBillers(Context context) {
        SQLiteDatabase db = SenzorsDbHelper.getInstance(context).getReadableDatabase();
        Cursor cursor = db.query(SenzorsDbContract.Biller.TABLE_NAME, // table
                null, // columns
                null, // constraint
                null, // params
                null, // order by
                null, // group by
                null); // join

        ArrayList<Biller> billers = new ArrayList<>();

        while (cursor.moveToNext()) {
            String type = cursor.getString(cursor.getColumnIndex(SenzorsDbContract.Biller.COLUMN_NAME_BILL_TYPE));
            String name = cursor.getString(cursor.getColumnIndex(SenzorsDbContract.Biller.COLUMN_NAME_BILLER_NAME));
            String account = cursor.getString(cursor.getColumnIndex(SenzorsDbContract.Biller.COLUMN_NAME_BILLER_ACCOUNT));

            Biller biller = new Biller();
            biller.setBillType(type);
            biller.setBillerName(name);
            biller.setBillerAccount(account);
            billers.add(biller);
        }
        cursor.close();

        return billers;
    }

    public static void deleteBiller(Context context, Biller biller) {
        SQLiteDatabase db = SenzorsDbHelper.getInstance(context).getWritableDatabase();

        // delete senz of given user
        db.delete(SenzorsDbContract.Biller.TABLE_NAME,
                SenzorsDbContract.Biller.COLUMN_NAME_BILLER_ACCOUNT + " = ?",
                new String[]{biller.getBillerAccount()});
    }
}
