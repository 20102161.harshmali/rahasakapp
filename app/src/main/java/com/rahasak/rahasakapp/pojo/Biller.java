package com.rahasak.rahasakapp.pojo;

/**
 * Created by eranga on 11/8/18.
 */

public class Biller {
    private String billType;
    private String billerName;
    private String billerAccount;

    public Biller() {

    }

    public String getBillType() {
        return billType;
    }

    public void setBillType(String billType) {
        this.billType = billType;
    }

    public String getBillerName() {
        return billerName;
    }

    public void setBillerName(String billerName) {
        this.billerName = billerName;
    }

    public String getBillerAccount() {
        return billerAccount;
    }

    public void setBillerAccount(String billerAccount) {
        this.billerAccount = billerAccount;
    }
}
