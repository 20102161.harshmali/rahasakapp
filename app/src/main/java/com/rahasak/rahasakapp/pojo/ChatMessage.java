package com.rahasak.rahasakapp.pojo;

import android.os.Parcel;
import android.os.Parcelable;

public class ChatMessage implements Parcelable {
    String id;
    String body;
    Boolean isMyMessage;
    Long timestamp;

    public ChatMessage() {
    }

    protected ChatMessage(Parcel in) {
        id = in.readString();
        body = in.readString();
        byte tmpIsMyMessage = in.readByte();
        isMyMessage = tmpIsMyMessage == 0 ? null : tmpIsMyMessage == 1;
        timestamp = in.readLong();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(body);
        dest.writeByte((byte) (isMyMessage == null ? 0 : isMyMessage ? 1 : 2));
        dest.writeLong(timestamp);
    }

    public static final Creator<ChatMessage> CREATOR = new Creator<ChatMessage>() {
        @Override
        public ChatMessage createFromParcel(Parcel in) {
            return new ChatMessage(in);
        }

        @Override
        public ChatMessage[] newArray(int size) {
            return new ChatMessage[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Boolean getIsMyMessage() {
        return isMyMessage;
    }

    public void setIsMyMessage(Boolean isMyMessage) {
        this.isMyMessage = isMyMessage;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public int describeContents() {
        return 0;
    }

}
