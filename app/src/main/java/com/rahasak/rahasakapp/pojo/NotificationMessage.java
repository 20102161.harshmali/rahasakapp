package com.rahasak.rahasakapp.pojo;

import android.os.Parcel;
import android.os.Parcelable;

public class NotificationMessage implements Parcelable {
    private String promizeId;
    private String promizeUser;
    private String promizeAmount;
    private String promizeSalt;
    private String promizeStatus;

    public NotificationMessage() {
    }

    protected NotificationMessage(Parcel in) {
        promizeId = in.readString();
        promizeUser = in.readString();
        promizeAmount = in.readString();
        promizeSalt = in.readString();
        promizeStatus = in.readString();
    }

    public static final Creator<NotificationMessage> CREATOR = new Creator<NotificationMessage>() {
        @Override
        public NotificationMessage createFromParcel(Parcel in) {
            return new NotificationMessage(in);
        }

        @Override
        public NotificationMessage[] newArray(int size) {
            return new NotificationMessage[size];
        }
    };

    public String getPromizeId() {
        return promizeId;
    }

    public void setPromizeId(String promizeId) {
        this.promizeId = promizeId;
    }

    public String getPromizeUser() {
        return promizeUser;
    }

    public void setPromizeUser(String promizeUser) {
        this.promizeUser = promizeUser;
    }

    public String getPromizeAmount() {
        return promizeAmount;
    }

    public void setPromizeAmount(String promizeAmount) {
        this.promizeAmount = promizeAmount;
    }

    public String getPromizeSalt() {
        return promizeSalt;
    }

    public void setPromizeSalt(String promizeSalt) {
        this.promizeSalt = promizeSalt;
    }

    public String getPromizeStatus() {
        return promizeStatus;
    }

    public void setPromizeStatus(String promizeStatus) {
        this.promizeStatus = promizeStatus;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(promizeId);
        dest.writeString(promizeUser);
        dest.writeString(promizeAmount);
        dest.writeString(promizeSalt);
        dest.writeString(promizeStatus);
    }
}
