package com.rahasak.rahasakapp.util;

import org.json.JSONException;

import java.util.HashMap;
import java.util.Random;

public class BixUtil {

    public static String getRand(String seed, int length) {
        Random r = new Random();
        r.setSeed(seed.length());

        int m = (int) Math.pow(length, length - 1);
        int no = m + new Random().nextInt(9 * m);
        return Integer.toString(no);
    }

    public static String getBixIdentity(String email, String mobileNo, String ip) throws JSONException {
        HashMap<String, String> createMap = new HashMap<>();
        String bixId = BixUtil.getRand(email, 10);
        createMap.put("bixID", bixId);
        createMap.put("validated", "0");
        createMap.put("DN", "---------------------");
        createMap.put("countryName", "");
        createMap.put("state", "");
        createMap.put("localityName", "");
        createMap.put("organizationName", "");
        createMap.put("OU", "");
        createMap.put("commonName", email);
        createMap.put("altNameURL", email);
        createMap.put("altNameIP", ip);
        createMap.put("PII", "---------------------");
        createMap.put("firstName", "");
        createMap.put("middleName", "");
        createMap.put("lastName", "");
        createMap.put("email", email);
        createMap.put("mobileNumber", mobileNo);
        createMap.put("address1", "");
        createMap.put("address2", "");
        createMap.put("city", "");
        createMap.put("DoB", "");
        createMap.put("gender", "");
        createMap.put("nationalIDNo", "");
        createMap.put("drivingLicenseState", "");
        createMap.put("drivingLicenseNo", "");
        createMap.put("nationality", "");
        createMap.put("passportNo", "");
        createMap.put("facebookPage", "");
        createMap.put("linkedInURL", "");
        createMap.put("skypeId", "");
        createMap.put("twiterAccount", "");

        return JsonUtil.toJsonContract(createMap);
    }

    public static String getBixCertificateRequest(String bixIdentity, String publicKey) {
        String pemKey = "-----BEGIN PUBLIC KEY-----" + publicKey + "-----END PUBLIC KEY-----";
        String extendedPKCS10Request = bixIdentity + "<x>" + pemKey;
        String bixCertificateRequestMessage = "pkcs10WalletPortalCertificateRequest<->" + extendedPKCS10Request + "<end>";

        return bixCertificateRequestMessage;
    }

    public static String getBixIdentityObject(String email, String mobileNo, String bixId, String payload) throws JSONException {
        HashMap<String, String> createMap = new HashMap<>();
        createMap.put("seqNo", "");
        createMap.put("timeStamp", "");
        createMap.put("applicationID", "BIXLedger");
        createMap.put("objectType", "BIX Wallet");
        createMap.put("subjectEmail", email);
        createMap.put("subjectMobileNo", mobileNo);
        createMap.put("subjectURL", "");
        createMap.put("subjectBIXID", bixId);
        createMap.put("receiverEmail", email);
        createMap.put("receiverMobileNo", mobileNo);
        createMap.put("receiverURL", "");
        createMap.put("receiverBIXID", bixId);
        createMap.put("payload", payload);
        createMap.put("hashOfPreviousObject", "");
        createMap.put("signatureOfTheObject", "");
        createMap.put("validationCertificate", "");

        return JsonUtil.toJsonContract(createMap);
    }

    public static String getAppendLedgerRequest(String bixIdentityObject) {
        String request = "bixIdentityObject<->login<->" + bixIdentityObject + "<end>";

        return request;
    }

    public static String getPartnerRegisterRequest(String requester, String partner) {
        String request = "registerpartner<->requesteremail=" + requester + ";partneremail=" + partner + "<end>";

        return request;
    }

}
