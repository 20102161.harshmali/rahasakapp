package com.rahasak.rahasakapp.util;

import android.content.Context;
import android.util.Base64;

import org.spongycastle.cert.jcajce.JcaCertStore;
import org.spongycastle.cms.CMSAlgorithm;
import org.spongycastle.cms.CMSEnvelopedData;
import org.spongycastle.cms.CMSEnvelopedDataGenerator;
import org.spongycastle.cms.CMSException;
import org.spongycastle.cms.CMSProcessableByteArray;
import org.spongycastle.cms.CMSSignedData;
import org.spongycastle.cms.CMSSignedDataGenerator;
import org.spongycastle.cms.CMSTypedData;
import org.spongycastle.cms.KeyTransRecipientInformation;
import org.spongycastle.cms.RecipientInformation;
import org.spongycastle.cms.jcajce.JcaSignerInfoGeneratorBuilder;
import org.spongycastle.cms.jcajce.JceCMSContentEncryptorBuilder;
import org.spongycastle.cms.jcajce.JceKeyTransEnvelopedRecipient;
import org.spongycastle.cms.jcajce.JceKeyTransRecipient;
import org.spongycastle.cms.jcajce.JceKeyTransRecipientInfoGenerator;
import org.spongycastle.crypto.digests.RIPEMD160Digest;
import org.spongycastle.jce.provider.BouncyCastleProvider;
import org.spongycastle.operator.ContentSigner;
import org.spongycastle.operator.OutputEncryptor;
import org.spongycastle.operator.jcajce.JcaContentSignerBuilder;
import org.spongycastle.operator.jcajce.JcaDigestCalculatorProviderBuilder;
import org.spongycastle.util.Store;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.Signature;
import java.security.SignatureException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * RSR encryption
 */
public class CryptoUtil {

    // keys
    private static final String ZWITCH_KEY = "44323232";
    private static final String CHAINZ_KEY = "232ewewe";

    // size of RSA keys
    private static final int RSA_KEY_SIZE = 2048;
    private static final int SESSION_KEY_SIZE = 128;

    public static void initKeys(Context context) throws NoSuchProviderException, NoSuchAlgorithmException {
        // generate keypair
        KeyPairGenerator keyPairGenerator;
        keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        keyPairGenerator.initialize(RSA_KEY_SIZE, new SecureRandom());
        KeyPair keyPair = keyPairGenerator.generateKeyPair();

        // save keypair keys in shared preferences
        savePublicKey(context, keyPair);
        savePrivateKey(context, keyPair);

        // save zwitch/chainz key
        PreferenceUtil.put(context, PreferenceUtil.ZWITCH_KEY, CryptoUtil.ZWITCH_KEY);
        PreferenceUtil.put(context, PreferenceUtil.CHAINZ_KEY, CryptoUtil.CHAINZ_KEY);
    }

    private static void savePublicKey(Context context, KeyPair keyPair) {
        // get public key from keypair
        byte[] keyContent = keyPair.getPublic().getEncoded();
        String publicKey = Base64.encodeToString(keyContent, Base64.DEFAULT).replaceAll("\n", "").replaceAll("\r", "");

        // save public key in shared preference
        PreferenceUtil.put(context, PreferenceUtil.PUBLIC_KEY, publicKey);
    }

    private static void savePrivateKey(Context context, KeyPair keyPair) {
        // get public key from keypair
        byte[] keyContent = keyPair.getPrivate().getEncoded();
        String privateKey = Base64.encodeToString(keyContent, Base64.DEFAULT).replaceAll("\n", "").replaceAll("\r", "");

        // save private key in shared preference
        PreferenceUtil.put(context, PreferenceUtil.PRIVATE_KEY, privateKey);
    }

    public static PublicKey getPublicKey(Context context) throws InvalidKeySpecException, NoSuchAlgorithmException, NoSuchProviderException {
        // get key string from shared preference
        String keyString = PreferenceUtil.get(context, PreferenceUtil.PUBLIC_KEY);

        // convert to string key public key
        X509EncodedKeySpec spec = new X509EncodedKeySpec(Base64.decode(keyString, Base64.DEFAULT));
        KeyFactory kf = KeyFactory.getInstance("RSA");

        return kf.generatePublic(spec);
    }

    public static PublicKey getPublicKey(String keyString) throws InvalidKeySpecException, NoSuchAlgorithmException, NoSuchProviderException {
        // convert to string key public key
        X509EncodedKeySpec spec = new X509EncodedKeySpec(Base64.decode(keyString, Base64.DEFAULT));
        KeyFactory kf = KeyFactory.getInstance("RSA");

        return kf.generatePublic(spec);
    }

    public static PrivateKey getPrivateKey(Context context) throws InvalidKeySpecException, NoSuchAlgorithmException {
        // get key string from shared preference
        String keyString = PreferenceUtil.get(context, PreferenceUtil.PRIVATE_KEY);

        // convert to string key public key
        PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(Base64.decode(keyString, Base64.DEFAULT));
        KeyFactory kf = KeyFactory.getInstance("RSA");

        return kf.generatePrivate(spec);
    }

    public static String getZaddress(Context context) throws NoSuchAlgorithmException {
        // get public key
        byte[] key = Base64.decode(PreferenceUtil.get(context, PreferenceUtil.PUBLIC_KEY), Base64.DEFAULT);

        // generate digest
        byte[] ph = new byte[20];
        byte[] sha256 = MessageDigest.getInstance("SHA-256").digest(key);
        RIPEMD160Digest digest = new RIPEMD160Digest();
        digest.update(sha256, 0, sha256.length);
        digest.doFinal(ph, 0);

        // encode base58
        return Base58.encode(ph);
    }

    public static String getDigitalSignature(String payload, PrivateKey privateKey) throws SignatureException, InvalidKeyException, NoSuchAlgorithmException {
        // reformat payload
        String fPayload = payload.replaceAll(" ", "")
                .replaceAll("\n", "")
                .replaceAll("\r", "")
                .trim();
        Signature signature = Signature.getInstance("SHA256withRSA");
        signature.initSign(privateKey);
        signature.update(fPayload.getBytes());


        // Base64 encoded string
        return Base64.encodeToString(signature.sign(), Base64.DEFAULT).replaceAll("\n", "").replaceAll("\r", "");
    }

    public static boolean verifyDigitalSignature(String payload, String signedPayload, PublicKey publicKey) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException {
        String fPayload = payload.replaceAll(" ", "")
                .replaceAll("\n", "")
                .replaceAll("\r", "")
                .trim();
        Signature signature = Signature.getInstance("SHA256withRSA");
        signature.initVerify(publicKey);
        signature.update(fPayload.getBytes());

        byte[] signedPayloadContent = Base64.decode(signedPayload, Base64.DEFAULT);

        return signature.verify(signedPayloadContent);
    }

    public static String getSessionKey() throws NoSuchAlgorithmException {
        KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
        keyGenerator.init(SESSION_KEY_SIZE);
        SecretKey secretKey = keyGenerator.generateKey();
        return Base64.encodeToString(secretKey.getEncoded(), Base64.DEFAULT);
    }

    public static SecretKey getSecretKey(String encodedKey) {
        byte[] key = Base64.decode(encodedKey, Base64.DEFAULT);
        return new SecretKeySpec(key, 0, key.length, "AES");
    }

    public static byte[] getSalt(String key) {
        return new StringBuilder(key.substring(0, 12)).reverse().toString().getBytes();
    }

    public static String encryptRSA(PublicKey publicKey, String payload) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);

        byte[] data = cipher.doFinal(payload.getBytes());
        return Base64.encodeToString(data, Base64.DEFAULT);
    }

    public static String decryptRSA(PrivateKey privateKey, String payload) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.DECRYPT_MODE, privateKey);

        byte[] data = Base64.decode(payload, Base64.DEFAULT);
        return new String(cipher.doFinal(data));
    }

    public static String encryptECB(SecretKey secretKey, String payload) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException, NoSuchProviderException, InvalidAlgorithmParameterException, UnsupportedEncodingException {
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);

        byte[] data = cipher.doFinal(payload.getBytes());
        return Base64.encodeToString(data, Base64.DEFAULT);
    }

    public static String decryptECB(SecretKey secretKey, String payload) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException, NoSuchProviderException, InvalidAlgorithmParameterException, UnsupportedEncodingException {
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, secretKey);

        byte[] data = Base64.decode(payload, Base64.DEFAULT);
        return new String(cipher.doFinal(data));
    }

    public static byte[] encryptECB(SecretKey secretKey, byte[] payload, int offset, int lenght) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);

        return cipher.doFinal(payload, offset, lenght);
    }

    public static byte[] decryptECB(SecretKey secretKey, byte[] payload) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException, NoSuchProviderException, UnsupportedEncodingException, InvalidAlgorithmParameterException {
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, secretKey);

        return cipher.doFinal(payload);
    }

    public static String encryptCCM(SecretKey secretKey, String salt, String payload) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException, NoSuchProviderException, InvalidAlgorithmParameterException, UnsupportedEncodingException {
        Cipher cipher = Cipher.getInstance("AES/CCM/NoPadding", "BC");
        cipher.init(Cipher.ENCRYPT_MODE, secretKey, new IvParameterSpec(salt.getBytes("UTF-8")));

        byte[] data = cipher.doFinal(payload.getBytes());
        return Base64.encodeToString(data, Base64.DEFAULT);
    }

    public static String decryptCCM(SecretKey secretKey, String salt, String payload) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException, NoSuchProviderException, InvalidAlgorithmParameterException, UnsupportedEncodingException {
        Cipher cipher = Cipher.getInstance("AES/CCM/NoPadding", "BC");
        cipher.init(Cipher.DECRYPT_MODE, secretKey, new IvParameterSpec(salt.getBytes("UTF-8")));

        byte[] data = Base64.decode(payload, Base64.DEFAULT);
        return new String(cipher.doFinal(data));
    }

    public static byte[] encryptCCM(SecretKey secretKey, byte[] salt, byte[] payload, int offset, int lenght) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException, NoSuchProviderException, UnsupportedEncodingException, InvalidAlgorithmParameterException {
        Cipher cipher = Cipher.getInstance("AES/CCM/NoPadding", "BC");
        cipher.init(Cipher.ENCRYPT_MODE, secretKey, new IvParameterSpec(salt));

        return cipher.doFinal(payload, offset, lenght);
    }

    public static byte[] decryptCCM(SecretKey secretKey, byte[] salt, byte[] payload) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException, NoSuchProviderException, UnsupportedEncodingException, InvalidAlgorithmParameterException {
        Cipher cipher = Cipher.getInstance("AES/CCM/NoPadding", "BC");
        cipher.init(Cipher.DECRYPT_MODE, secretKey, new IvParameterSpec(salt));

        return cipher.doFinal(payload);
    }

    public static String encryptGCM(SecretKey secretKey, byte[] salt, String payload) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException, NoSuchProviderException, InvalidAlgorithmParameterException, UnsupportedEncodingException {
        Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding", "BC");
        cipher.init(Cipher.ENCRYPT_MODE, secretKey, new IvParameterSpec(salt));

        byte[] data = cipher.doFinal(payload.getBytes());
        return Base64.encodeToString(data, Base64.DEFAULT);
    }

    public static String decryptGCM(SecretKey secretKey, byte[] salt, String payload) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException, NoSuchProviderException, InvalidAlgorithmParameterException, UnsupportedEncodingException {
        Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding", "BC");
        cipher.init(Cipher.DECRYPT_MODE, secretKey, new IvParameterSpec(salt));

        byte[] data = Base64.decode(payload, Base64.DEFAULT);
        return new String(cipher.doFinal(data));
    }

    public static byte[] encryptGCM(SecretKey secretKey, byte[] salt, byte[] payload, int offset, int lenght) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException, NoSuchProviderException, UnsupportedEncodingException, InvalidAlgorithmParameterException {
        Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding", "BC");
        cipher.init(Cipher.ENCRYPT_MODE, secretKey, new IvParameterSpec(salt));

        return cipher.doFinal(payload, offset, lenght);
    }

    public static byte[] decryptGCM(SecretKey secretKey, byte[] salt, byte[] payload) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException, NoSuchProviderException, UnsupportedEncodingException, InvalidAlgorithmParameterException {
        Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding", "BC");
        cipher.init(Cipher.DECRYPT_MODE, secretKey, new IvParameterSpec(salt));

        return cipher.doFinal(payload);
    }

    public static String hashSha256(String blob) throws NoSuchAlgorithmException {
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        digest.update(blob.getBytes());
        byte[] bytes = digest.digest();

        // convert to hex
        StringBuilder sb = new StringBuilder();
        for (byte b : bytes) {
            sb.append(String.format("%02x", b));
        }

        return sb.toString();
    }

    static String uuid() {
        return UUID.randomUUID().toString();
    }

    public static X509Certificate getCert(String certStr) throws CertificateException {
        String pem = "-----BEGIN CERTIFICATE-----\n" +
                "MIID+jCCAuKgAwIBAwICNTUwDQYJKoZIhvcNAQELBQAwgY0xCzAJBgNVBAYTAlVT\n" +
                "MQswCQYDVQQIEwJNRDESMBAGA1UEBxMJUm9ja3ZpbGxlMRkwFwYDVQQKExBCSVgg\n" +
                "TGVkZ2VyLCBJbmMuMRswGQYDVQQLExJCQ0kgQWRtaW5pc3RyYXRpb24xJTAjBgNV\n" +
                "BAMTHEJJWCBTb3V0aCBBZnJpY2EgTGVkZ2VyIE5vZGUwHhcNMjAwODI2MDQwNjQ3\n" +
                "WhcNMjEwODI2MDQwNjQ3WjBOMQkwBwYDVQQGEwAxCTAHBgNVBAgTADEJMAcGA1UE\n" +
                "BxMAMQkwBwYDVQQKEwAxCTAHBgNVBAsTADEVMBMGA1UEAxMMaXNzYUBvcHMuY29t\n" +
                "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwpw/K+l3pdd1ZVfIVS9J\n" +
                "xkLFdQj4p7zufJiOJfBcxkp/jLOXjy/WgCKCEpZ+wB6M44jYrirXQTDuUwUhqByn\n" +
                "1CLpDA2WToLWMkulbOTIPBn1Gu+6DfFlwYvV/Mbh14I1K9YigNmIK8Qnfgn0S0EN\n" +
                "U1xYiGsvvna2xX4X1TKSd8SoUcqfLb5Ix4KEAF3M0Kh/VcdHtUe+i/aZAg8af87v\n" +
                "lJ4/rOj2SfRKvZxgiaY6netagIdaQ2YMUm72ZiCv6FkCiIs79E9ilQ76dqY2xUHU\n" +
                "O5IbNUHEGDgHJJ3s7YW4WGPF56zG0DnuT5QSUZCGpJmxjtE86WTlFxKFhV2n3d3I\n" +
                "7QIDAQABo4GhMIGeMAkGA1UdEwQCMAAwCwYDVR0PBAQDAgL0MDsGA1UdJQQ0MDIG\n" +
                "CCsGAQUFBwMBBggrBgEFBQcDAgYIKwYBBQUHAwMGCCsGAQUFBwMEBggrBgEFBQcD\n" +
                "CDARBglghkgBhvhCAQEEBAMCAPcwHQYDVR0RBBYwFIYMaXNzYUBvcHMuY29thwTA\n" +
                "qAwKMBUGA1UdDgQOBAxpc3NhQG9wcy5jb20wDQYJKoZIhvcNAQELBQADggEBAD7j\n" +
                "thiDz4P1MxUeegjaWFPeT1+GH46cjFIfLUtrFwmXIl11cHoeHezlhEAodQjmxAur\n" +
                "5AWox9kI9wX78qmKYAiRpemk1fTUZ1MmYuoBGqP8nX7lvq1Aw2zJdLElmT9OkxHA\n" +
                "2H0R31kJ6AbdKRuIFBa+rKv5h/T5l55ticmuCoI+NpFz30dcw11iON6DmtUoIKTT\n" +
                "HVJeZo8KGmiyBRgAwBLbDMovmBIqmA9IQSBDDpZZxWctOinMGc4tJNGWpNhpinfn\n" +
                "wpd9pUO/GOJg1J09jHQ/RucJ8qM5TuDGietrZr4X0p+5BehLaOrRKRj2Ltrc0Nvx\n" +
                "HIV5Taptt0ekvKmnYEs=\n" +
                "-----END CERTIFICATE-----\n";

        String pem1 = "-----BEGIN CERTIFICATE-----\n" +
                "MIIDPjCCAiagAwIBAgIJAPvd1gx14C3CMA0GCSqGSIb3DQEBBQUAMEcxCzAJBgNV\n" +
                "BAYTAk1BMRAwDgYDVQQIEwdNb3JvY2NvMRMwEQYDVQQHEwpDYXNhYmxhbmNhMREw\n" +
                "DwYDVQQDEwhCYWVsZHVuZzAeFw0xNzEwMTIxMDQzMTRaFw0yNzEwMTMxMDQzMTRa\n" +
                "MEcxCzAJBgNVBAYTAk1BMRAwDgYDVQQIEwdNb3JvY2NvMRMwEQYDVQQHEwpDYXNh\n" +
                "YmxhbmNhMREwDwYDVQQDEwhCYWVsZHVuZzCCASIwDQYJKoZIhvcNAQEBBQADggEP\n" +
                "ADCCAQoCggEBAMyi5GmOeN4QaH/CP5gSOyHX8znb5TDHWV8wc+ZT7kNU8zt5tGMh\n" +
                "jozK6hax155/6tOsBDR0rSYBhL+Dm/+uCVS7qOlRHhf6cNGtzGF1gnNJB2WjI8oM\n" +
                "AYm24xpLj1WphKUwKrn3nTMPnQup5OoNAMYl99flANrRYVjjxrLQvDZDUio6Iujr\n" +
                "CZ2TtXGM0g/gP++28KT7g1KlUui3xtB0u33wx7UN8Fix3JmjOaPHGwxGpwP3VGSj\n" +
                "fs8cuhqVwRQaZpCOoHU/P8wpXKw80sSdhz+SRueMPtVYqK0CiLL5/O0h0Y3le4IV\n" +
                "whgg3KG1iTGOWn60UMFn1EYmQ18k5Nsma6UCAwEAAaMtMCswCQYDVR0TBAIwADAR\n" +
                "BglghkgBhvhCAQEEBAMCBPAwCwYDVR0PBAQDAgUgMA0GCSqGSIb3DQEBBQUAA4IB\n" +
                "AQC8DDBmJ3p4xytxBiE0s4p1715WT6Dm/QJHp0XC0hkSoyZKDh+XVmrzm+J3SiW1\n" +
                "vpswb5hLgPo040YX9jnDmgOD+TpleTuKHxZRYj92UYWmdjkWLVtFMcvOh+gxBiAP\n" +
                "pHIqZsqo8lfcyAuh8Jx834IXbknfCUtERDLG/rU9P/3XJhrM2GC5qPQznrW4EYhU\n" +
                "CGPyIJXmvATMVvXMWCtfogAL+n42vjYXQXZoAWomHhLHoNbSJUErnNdWDOh4WoJt\n" +
                "XJCxA6U5LSBplqb3wB2hUTqw+0admKltvmy+KA1PD7OxoGiY7V544zeGqJam1qxU\n" +
                "ia7y5BL6uOa/4ShSV8pcJDYz\n" +
                "-----END CERTIFICATE-----";

        String pemf = pem.replaceAll("\n", "")
                .replaceAll("\r", "")
                .replaceAll("-----BEGIN CERTIFICATE-----", "")
                .replaceAll("-----END CERTIFICATE-----", "");

        byte[] decoded = Base64.decode(pemf, Base64.DEFAULT);
        X509Certificate certificate = (X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(decoded));

        return certificate;
    }

    public static PrivateKey getKey(String keyStry) throws InvalidKeySpecException, NoSuchAlgorithmException {
        String key = "-----BEGIN RSA PRIVATE KEY-----\n" +
                "MIIEpAIBAAKCAQEAzKLkaY543hBof8I/mBI7IdfzOdvlMMdZXzBz5lPuQ1TzO3m0\n" +
                "YyGOjMrqFrHXnn/q06wENHStJgGEv4Ob/64JVLuo6VEeF/pw0a3MYXWCc0kHZaMj\n" +
                "ygwBibbjGkuPVamEpTAqufedMw+dC6nk6g0AxiX31+UA2tFhWOPGstC8NkNSKjoi\n" +
                "6OsJnZO1cYzSD+A/77bwpPuDUqVS6LfG0HS7ffDHtQ3wWLHcmaM5o8cbDEanA/dU\n" +
                "ZKN+zxy6GpXBFBpmkI6gdT8/zClcrDzSxJ2HP5JG54w+1ViorQKIsvn87SHRjeV7\n" +
                "ghXCGCDcobWJMY5afrRQwWfURiZDXyTk2yZrpQIDAQABAoIBAQDJ+QxQUtpg8vjx\n" +
                "ABwcUAIlCSt7M9omJtvC4+PAtZ44vqPDVAACx6AWvkAFXkpemgqAGQudantrQ5xu\n" +
                "Gcviszdqdj3k6rc1G242xb8vgffbKX0vh7FPPkJWVjgYP/OcrM/K6puBKS/ysbpj\n" +
                "RLA3gdX9vIO+hrwtPPND9Kys2PCq7Es4ZiwQrpE5BWkxwngrPLTSPZvezKiFhxrg\n" +
                "52ti7uW1svpbxlfhZVfoEmwgXZVlYKEjNCYH+RS8nGbySs4065aQ9vNBmMuZBWTn\n" +
                "k90LfdCDxfJVvulMEV000637IBFXSbZkvWtkFBdj+4e56rECbKq8evBq6LZvjbQ2\n" +
                "Kg/6t3WBAoGBAOr8o1IPpudVQlMlFEhgeEoH7NPaeF1WRIuMDycwxbPgbfrEoeMJ\n" +
                "m0wZSDm6XcJWnJnO6jsyXKJTTKVzAWixxO2hs6+bgEHnGZtJYcjwn33Hx5+5N3hj\n" +
                "1Wygh4fVYr04ariiq1byZWJrX8wJdxPpmpeWm+1HDlSEUKkd38hIbU+ZAoGBAN7v\n" +
                "dqlfBqvSepElNjcGmUX3S0iT1KHacDJEcbyp+wPca3tF376xtZBef/7XDozD85Nj\n" +
                "y2E97onTBS3Qny/4Gh/uFuAZcvOhFINLO6a+4OiEip/6q5hZH2aDnqCedxxHdGlM\n" +
                "SVsPkoDK5nljkLghUYG+EtwuNvuJhg29wSM0AnPtAoGBANbYxjpxNRitS0bZ0AGc\n" +
                "sl59gHrydLph9anhFLmwThJ4UBXHUnxSfDrHotGlI7RbnDLEjEQzd56yqpFa5R7j\n" +
                "6jmabr9QXaPjt/XS0iT2W9yUnF/c6GqaXLhEzaXT48M9odXOZPnKBghGRAA5ZzY9\n" +
                "j6qpeUcogGGl2FnHhqOIFOmBAoGAFEfgmaPlbXgmYlHqTrMw+mZ22NFqBJdkoY4M\n" +
                "csdb6IwC0yIWKrjr9YhDSKGz1yZ0YDIx7FTAAJXL4NEbyjdd2Q3F0vk9teAvh4ou\n" +
                "kQrIVx7HpaoeNivVaXQL2DzSRSZS9slT2w7dmw3aTay7G6UcTxtFh/ZEVjm6kkFy\n" +
                "8ormtjkCgYADB2vumwAKkqpYOXNjWSu4FMeH6+9nzJJxjKuij0sJHbJCeHmRHEJw\n" +
                "6WgpO9Nststaulb5thuFs5jEga3wuZracfhuGApwZHkypVUAA8UqzDW8kHIjXxV5\n" +
                "Mvywt89M503aHSAmFWl0NxyHLc+2K2E8b+2ssVYBSFZ/+BNZPtBLOg==\n" +
                "-----END RSA PRIVATE KEY-----\n";
        String keyf = key.replaceAll("\n", "")
                .replaceAll("\r", "")
                .replaceAll("-----BEGIN RSA PRIVATE KEY-----", "")
                .replaceAll("-----END RSA PRIVATE KEY-----", "");

        // convert to string key private key
        PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(Base64.decode(keyf, Base64.DEFAULT));
        KeyFactory kf = KeyFactory.getInstance("RSA");

        return kf.generatePrivate(spec);
    }

    public static String pkcs7Envelop(X509Certificate cert, String message) throws CertificateEncodingException, IOException, CMSException {
        String msg = "{\"countryName\":\"\",\"skypeId\":\"\",\"address2\":\"\",\"city\":\"\",\"address1\":\"\",\"altNameIP\":\"192.168.12.10\",\"gender\":\"\",\"OU\":\"\",\"firstName\":\"\",\"DoB\":\"\",\"middleName\":\"\",\"passportNo\":\"\",\"twiterAccount\":\"\",\"mobileNumber\":\"0775432011\",\"validated\":\"0\",\"altNameURL\":\"e@ops.com\",\"commonName\":\"e@ops.com\",\"linkedInURL\":\"\",\"organizationName\":\"\",\"facebookPage\":\"\",\"drivingLicenseState\":\"\",\"email\":\"e@ops.com\",\"state\":\"\",\"nationality\":\"\",\"drivingLicenseNo\":\"\",\"DN\":\"---------------------\",\"localityName\":\"\",\"PII\":\"---------------------\",\"lastName\":\"\",\"nationalIDNo\":\"\",\"bixID\":\"1094977166\"}";
        CMSEnvelopedDataGenerator gen = new CMSEnvelopedDataGenerator();
        gen.addRecipientInfoGenerator(new JceKeyTransRecipientInfoGenerator(cert).setProvider("BC"));
        CMSTypedData data = new CMSProcessableByteArray(msg.getBytes("UTF-8"));
        CMSEnvelopedData enveloped = gen.generate(data, new JceCMSContentEncryptorBuilder(CMSAlgorithm.DES_EDE3_CBC).setProvider("BC").build());
        return Base64.encodeToString(enveloped.getEncoded(), Base64.DEFAULT);
    }

    public static String pkcs7Encrypt(X509Certificate cert, String message) throws CertificateEncodingException, IOException, CMSException {
        String msg = "{\"countryName\":\"\",\"skypeId\":\"\",\"address2\":\"\",\"city\":\"\",\"address1\":\"\",\"altNameIP\":\"192.168.12.10\",\"gender\":\"\",\"OU\":\"\",\"firstName\":\"\",\"DoB\":\"\",\"middleName\":\"\",\"passportNo\":\"\",\"twiterAccount\":\"\",\"mobileNumber\":\"0775432011\",\"validated\":\"0\",\"altNameURL\":\"e@ops.com\",\"commonName\":\"g@j.com\",\"linkedInURL\":\"\",\"organizationName\":\"\",\"facebookPage\":\"\",\"drivingLicenseState\":\"\",\"email\":\"g@j.com\",\"state\":\"\",\"nationality\":\"\",\"drivingLicenseNo\":\"\",\"DN\":\"---------------------\",\"localityName\":\"\",\"PII\":\"---------------------\",\"lastName\":\"\",\"nationalIDNo\":\"\",\"bixID\":\"1094977166\"}";
        CMSEnvelopedDataGenerator gen = new CMSEnvelopedDataGenerator();
        JceKeyTransRecipientInfoGenerator jceKey = new JceKeyTransRecipientInfoGenerator(cert);
        gen.addRecipientInfoGenerator(jceKey);
        CMSTypedData cmsData = new CMSProcessableByteArray(msg.getBytes("UTF-8"));
        OutputEncryptor encryptor = new JceCMSContentEncryptorBuilder(CMSAlgorithm.AES128_CBC).setProvider("BC").build();
        CMSEnvelopedData cmsEnvelopedData = gen.generate(cmsData, encryptor);
        return Base64.encodeToString(cmsEnvelopedData.getEncoded(), Base64.DEFAULT).replaceAll("\n", "").replaceAll("\r", "");
    }

    public static String pkcs7Decrypt(PrivateKey privateKey, String message) throws IOException, CMSException {
        byte[] decoded = Base64.decode(message, Base64.DEFAULT);
        CMSEnvelopedData envelopedData = new CMSEnvelopedData(decoded);
        Collection<RecipientInformation> recipients = envelopedData.getRecipientInfos().getRecipients();
        KeyTransRecipientInformation recipientInfo = (KeyTransRecipientInformation) recipients.iterator().next();
        JceKeyTransRecipient recipient = new JceKeyTransEnvelopedRecipient(privateKey);

        byte[] decrypted = recipientInfo.getContent(recipient);
        return new String(decrypted, "UTF-8");
    }

    public static String pkcs7Sign(X509Certificate signingCertificate, PrivateKey signingKey, String message) throws Exception {
        List<X509Certificate> certList = new ArrayList<>();
        CMSTypedData cmsData = new CMSProcessableByteArray(message.getBytes("UTF-8"));
        certList.add(signingCertificate);
        Store certs = new JcaCertStore(certList);

        CMSSignedDataGenerator cmsGenerator = new CMSSignedDataGenerator();
        ContentSigner contentSigner = new JcaContentSignerBuilder("SHA256withRSA").build(signingKey);
        cmsGenerator.addSignerInfoGenerator(
                new JcaSignerInfoGeneratorBuilder(new JcaDigestCalculatorProviderBuilder().setProvider("BC").build())
                        .build(contentSigner, signingCertificate));
        cmsGenerator.addCertificates(certs);

        CMSSignedData cms = cmsGenerator.generate(cmsData, true);
        return Base64.encodeToString(cms.getEncoded(), Base64.DEFAULT).replaceAll("\n", "").replaceAll("\r", "");
    }

    public static byte[] encryptData(byte[] data,
                                     X509Certificate encryptionCertificate)
            throws CertificateEncodingException, CMSException, IOException {
        Security.addProvider(new BouncyCastleProvider());

        byte[] encryptedData = null;
        if (null != data && null != encryptionCertificate) {
            CMSEnvelopedDataGenerator cmsEnvelopedDataGenerator
                    = new CMSEnvelopedDataGenerator();

            JceKeyTransRecipientInfoGenerator jceKey
                    = new JceKeyTransRecipientInfoGenerator(encryptionCertificate);
            cmsEnvelopedDataGenerator.addRecipientInfoGenerator(jceKey);
            CMSTypedData msg = new CMSProcessableByteArray(data);
            OutputEncryptor encryptor
                    = new JceCMSContentEncryptorBuilder(CMSAlgorithm.AES128_CBC)
                    .setProvider("BC").build();
            CMSEnvelopedData cmsEnvelopedData = cmsEnvelopedDataGenerator
                    .generate(msg, encryptor);
            encryptedData = cmsEnvelopedData.getEncoded();
        }
        System.out.println(Base64.encodeToString(encryptedData, Base64.DEFAULT));
        return encryptedData;
    }
}
